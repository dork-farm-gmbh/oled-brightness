use std::process::{Command};
use regex::Regex;
extern crate clap;
use clap::{Arg, App};

fn main() {
    let arg = Arg::with_name("direction")
      .short("d")
      .long("direction")
      .required(true)
      .takes_value(true)
      .possible_value("up")
      .possible_value("down")
      .case_insensitive(true);

    let matches = App::new("MyApp")
      .arg(arg)
      .get_matches(); // builds the instance of ArgMatches

    let mut xrandr = Command::new("xrandr");
    xrandr.arg("--verbose");
    let output = xrandr.output().expect("failed to execute process");
    let stdout_text = std::str::from_utf8(&output.stdout).unwrap();

    let re = Regex::new(r"Gamma:\s+(\d{1}\.\d{1,2}):(\d{1}\.\d{1,2}):(\d{1}\.\d{1,2}).*\n.*Brightness:\s(\d{1}\.\d{1,2})").unwrap();
    let current_red: f32 = 
      re.captures(stdout_text).unwrap()
        .get(1).unwrap()
        .as_str()
        .parse().unwrap();
    let new_red = 1.00 / current_red;
    let current_green: f32 = 
      re.captures(stdout_text).unwrap()
        .get(2).unwrap()
        .as_str()
        .parse().unwrap();
    let new_green = 1.00 / current_green;
    let current_blue: f32 = 
      re.captures(stdout_text).unwrap()
        .get(3).unwrap()
        .as_str()
        .parse().unwrap();
    let new_blue = 1.00 / current_blue;
    let current_brightness: f32 = 
        re.captures(stdout_text).unwrap()
            .get(4).unwrap()
            .as_str()
            .parse().unwrap();
    let mut direction = -1.0;
    if let Some(d) = matches.value_of("direction") {
        if d == "up" {
            direction = 1.0;
        }
    }
    let new_brightness = current_brightness + (0.1 * direction);
    let new_gamma = [new_red.to_string(), new_green.to_string(), new_blue.to_string()].join(":");
    // println!("gamma {}", new_gamma);
    Command::new("xrandr")
        .args(&["--output", "eDP-1", "--brightness", &new_brightness.to_string(), "--gamma", &new_gamma])
        .status()
        .expect("xrandr command failed to execute");
}
